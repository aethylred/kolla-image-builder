# GitLab-CI Kolla Image Builder

This repository contains a collection of Ruby scripts that can generate a `gitlab-ci.yml` file that when submitted to a GitLab instance with GitLab CI and GitLab runners set up with Docker in Docker support will build a collection of Kolla component container images that can be deployed by Kolla-Ansible. 

# General Usage

Clone or Fork the repository, then create a branch specifically for your intended Kolla-Ansible deployment. Modify the appropriate configuration file to only build the desired Kolla component images for the target OpenStack release and set them to `run_on_branch` for that branch. Remove `main` and `master` from the `run_on_branch` list.

Do not submit changes or merge requests to `develop`, `main` or `master` for the config yml files, template-overrides, or generated gitlab-ci.yml files. Only changes to the generator scripts and/or Kolla patches should be commited to `develop`, `main` or `master`.

# GitLab-CI by-release Pipeline

This modification uses release specific builder images with Kolla already configured, installed and patched. The reasoning here was that instead of repeating these tasks with each Kolla component image build, time could be saved by performing common tasks when creating the builder image rather than repeating them. This results in significant reduction of time building each image, however building source images still takes quite a while.

This generator can only use a [Kolla by-release builder image from Aethylred's GitLab](https://gitlab.com/aethylred/kolla-runner), or clone it build a an image on a local GitLab instance.

## Arguments

The `generate_pipeline_by_release.rb` accepts the path to another `by_release_config.yml` file as an argument, for example:

```
ruby generate_pipeline_by_release.rb custom_by_release_config.yml
```

## Trigger Build

Build of all releases is triggered by pushing changes to `master` or `main`, though it is recommended that builds be set to `manual`. If other branches are required change the `run_on_branch` key in the [`releases.yml`](releases.yml) file

## Add Component
When adding a new component to the build process, follow these steps:

1. Edit `by_release_config.yml`, add component's Kolla-compatible alias (usable with kolla-build as a target).
2. Run `ruby generate_pipeline_by_release.rb` to regenerate `.gitlab-ci.yml`.
3. Commit & push changes to run the pipeline.


# GitLab-CI Pipeline

This modification of the original script creates a diacyclic graph pipeline with the GitLab-CI `needs:` directive. This means that each component that is triggered can run independently of the other components, each with their own `build`, `test`, and `release` stage. This means that buildable components that are successful are not halted by failing components, and that shorter running builds are not held back by longer running builds.

This generator can use the original [Kolla runner builder image from MUNI](https://gitlab.ics.muni.cz/cloud/kolla-runner) or a [Kolla by-release builder image from Aethylred's GitLab](https://gitlab.com/aethylred/kolla-runner).

## Arguments

The `generate_gitlab_pipeline.rb` accepts the path to another `by_release_config.yml` file as an argument, for example:

```
ruby generate_gitlab_pipeline.rb custom_by_release_config.yml
```

## Trigger Build

Build of all releases is triggered by pushing changes to `master` or `main`, though it is recommended that builds be set to `manual`. If other branches are required change the `run_on_branch` key in the [`releases.yml`](releases.yml) file

## Add Component
When adding a new component to the build process, follow these steps:

1. Edit `pipeline_config.yml`, add component's Kolla-compatible alias (usable with kolla-build as a target).
2. Run `ruby generate_gitlab_pipeline.rb` to regenerate `.gitlab-ci.yml`.
3. Commit & push changes to run the pipeline.

# Original MUNI GitLab-CI 

These are the instructions for the original Kolla image building GitLab CI jobs from the [MUNI Kolla repository](https://gitlab.ics.muni.cz/cloud/kolla) by [Andrei Kirushchanka](https://gitlab.ics.muni.cz/andy1609)

This generator can use the original [Kolla runner builder image from MUNI](https://gitlab.ics.muni.cz/cloud/kolla-runner) or a [by-release builder image from Aethylred's GitLab](https://gitlab.com/aethylred/kolla-runner).

## Trigger Build

Build of all releases is triggered by pushing changes to `master` or `main`, though it is recommended that builds be set to `manual`. If other branches are required change the `run_on_branch` key in the [`releases.yml`](releases.yml) file

## Add Component
When adding a new component to the build process, follow these steps:

1. Edit `releases.yml`, add component's Kolla-compatible alias (usable with kolla-build as a target).
2. Run `ruby generate_gitlab_ci_yml.rb` to regenerate `.gitlab-ci.yml`.
3. Commit & push changes to run the pipeline.

# Override Component Templates

This feature applies to _all_ generator scripts.

If you need to add an override (change something in Kolla's j2 template for Dockerfile of the given component), follow
these steps:

1. Create `$COMPONENT_KOLLA_ALIAS.j2` in `template-overrides/$RELEASE` directory with the desired content.
2. Run `ruby generate_gitlab_ci_yml.rb` to regenerate `.gitlab-ci.yml`, or use one of the other generation scripts
3. Commit & push changes to run the pipeline.

`Notice:` Only named blocks in existing templates support the override mechanism! Content outside of
`{% block NAME %} ... {% endblock %}` CANNOT be overridden.

`Notice:` Overrides for common images (dependencies shared between components) MUST be added to override files of all
dependent components! Overrides for base images should be placed in `default.j2`.

For details on overriding templates, see official Kolla documentation. Or have a look at existing files in `template-overrides`.

## Keystone override
Find out parent id
 - https://gitlab.ics.muni.cz/cloud/kolla/blob/master/files/master/keystone/mapped.py.patch#L53
   - Will find out parent id for parent id creation for autocreated projects (federated users have autocreated projects)
 - https://gitlab.ics.muni.cz/cloud/kolla/blob/master/files/master/keystone/schema.py.patch
   - Schema update for enabling parent specification
 - https://gitlab.ics.muni.cz/cloud/kolla/blob/master/files/master/keystone/utils.py.patch#L7
   - Utils update to support parents
  

Project mapping update
 - https://gitlab.ics.muni.cz/cloud/kolla/blob/master/files/master/keystone/mapped.py.patch#L80
   - Find out current mapping (which projects does the user has access to)
 - https://gitlab.ics.muni.cz/cloud/kolla/blob/master/files/master/keystone/mapped.py.patch#L87
   - Mapping to be deleted - `[current_assignment] - [mapping_which_should_be_present]`
 - https://gitlab.ics.muni.cz/cloud/kolla/blob/master/files/master/keystone/mapped.py.patch#L110
   - Find all roles in a project to remove mapping from user (roles of user on 
     a specific project), delete all of them,
     - This is the problematic part, why the upstream may not include this ever.
       The lookup process takes too long (its cost efectivness was topic before,
       dont know whether anything has ever happend regarding this topic)

# Kolla patching

This feature only applies to the following GitLab-CI generator scripts:
- [`generate_gitlab_pipeline.rb`](generate_gitlab_pipeline.rb)
- [`generate_gitlab_ci.rb`](generate_gitlab_ci.rb)

This feature does not apply to the [`generate_pipeline_by_release.rb`](generate_pipeline_by_release.rb) script as it relies on patches to be applied to the release specific builder image.

This is a yet to be implemented feature, as it's kind of superceded by the `by_release` feature. It should be possible to patch Kolla before executing the component image builds.
 
# License

[Apache 2.0](LICENSE)

# References

This work is derived from [Kolla image builder](https://gitlab.ics.muni.cz/cloud/kolla) by [Andrei Kirushchanka](https://gitlab.ics.muni.cz/andy1609)