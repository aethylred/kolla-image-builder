# Version history

# 1.1.0
- Creates DAG pipelines for each component with `generate_pipeline_by_release.rb` wich uses a release specific builder image to speed up the Kolla component image builds.

# 1.0.0
- Creates DAG pipelines for each component with `generate_gitlab_pipeline.rb` and the playbook so slow or failing jobs don't hold up releasing images from working jobs.

# Before 1.0.0 (tagged as 0.1.0)
- Copied from [Kolla](https://gitlab.ics.muni.cz/cloud/kolla) which isn't Kolla itself but a Ruby script that creates a `gitlab-ci.yml` to build Kolla component container images.

# Branches

## by_release - use images specific for each OpenStack release

Modified the runner builder to pepare an image for each OpenStack release so that each Kolla component image doesn't have to repeat installing packages & modules and then build `kolla-build.conf` with `tox`

## CHUNKY - chunk image builds
The purpose of this change is to break the imagebuild into a number of parallel jobs of a small number of image each. This includes

- Branch selection: new feature specifying `run_on_branch` variable in [`releases.yaml`](releases.yaml) which gives a list of branches to run the build process on. This means that the image builder now handles the transition from [master to main](https://github.com/github/renaming) or using a non-default branch to trigger image builds. Default is set to trigger on both `main` and `master` branches.

- Chunking image building process: Building all these images can take a long time, so the goal is to set a `batch_size` variable and break up the list of components to build into batches with that number of components, or fewer. This should parallelise the build time across multiple GitLab workers, and make each job much shorter to execute.