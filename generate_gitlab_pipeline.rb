#!/usr/bin/env ruby

# -------------------------------------------------------------------------- #
# Licensed under the Apache License, Version 2.0 (the "License"); you may    #
# not use this file except in compliance with the License. You may obtain    #
# a copy of the License at                                                   #
#                                                                            #
# http://www.apache.org/licenses/LICENSE-2.0                                 #
#                                                                            #
# Unless required by applicable law or agreed to in writing, software        #
# distributed under the License is distributed on an "AS IS" BASIS,          #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   #
# See the License for the specific language governing permissions and        #
# limitations under the License.                                             #
#--------------------------------------------------------------------------- #

require 'yaml'
require 'erb'

INPUT_TEMPLATE = 'pipeline_yml.erb'.freeze
OUTPUT_FILE    = '.gitlab-ci.yml'.freeze

if ARGV.empty?
    CONFIG_FILE  = 'pipeline_config.yml'.freeze
else
    CONFIG_FILE = ARGV[0].freeze
end

config = YAML.load_file(CONFIG_FILE)
File.open(OUTPUT_FILE, 'w') { |file| file.write(ERB.new(File.read(INPUT_TEMPLATE), nil, '-').result) }
