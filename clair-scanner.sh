#!/bin/bash

IMAGE_LIST=$1

if test -r "$IMAGE_LIST" -a -f "$IMAGE_LIST"
then
  printf 'Scanning images:\n'
  cat "$IMAGE_LIST"
else
  printf 'Nothing to scan. Image list is missing or unreadable!\n'
  exit 1
fi

while IFS="" read -r IMAGE || [ -n "$IMAGE" ]
do
  printf 'Scanning %s\n' "$IMAGE"
  SHORT_NAME=$(basename "$IMAGE")
  docker pull "$IMAGE"
  /opt/clair/clair-scanner -c http://docker:6060 --ip "$(hostname -i)" \
                           -r "gl-container-scanning-report-${SHORT_NAME}.json" \
                           -l "clair-${SHORT_NAME}.log" \
                           -w clair-whitelist.yml \
                           "$IMAGE"
done < $IMAGE_LIST
